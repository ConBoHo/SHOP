// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
// import vueResource from "vue-resource"//ajaxm模块
// Vue.config
// Vue.use(vueResource)
// Vue.http.options.emulateJSON = true;

import VueAwesomeSwiper from 'vue-awesome-swiper';
Vue.use(VueAwesomeSwiper) //记得不要忘记这句

Vue.config.productionTip = false
router.push("home")
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
