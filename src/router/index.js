import Vue from 'vue'
import Router from 'vue-router'
// import Hello from '@/components/Hello'
import Home from '@/pages/home/'
import Gift from '@/pages/gift'
import Class from '@/pages/class'
import Shopcar from '@/pages/shopcar'
import My from '@/pages/my'
import Jcsh from '@/pages/Home/jcsh'
import Shsp from '@/pages/Home/shsp'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/home',
      name: 'home',
      component: Home,
      children:[
      {
      path:"/home/jcsh",
      component: Jcsh,
      },
      {
      path:"/home/shsp",
      component: Shsp,
      },
      ]
    },
    {
      path: '/gift',
      name: 'gift',
      component: Gift
    },
    {
      path: '/class',
      name: 'class',
      component: Class
    },
    {
      path: '/shopcar',
      name: 'shopcar',
      component: Shopcar
    },
    {
      path: '/my',
      name: 'my',
      component: My
    }
  ]
})
